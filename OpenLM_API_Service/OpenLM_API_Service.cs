﻿using APISamples;
using OpenLM_API_Service.AdminAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OpenLM_API_Service
{
    public partial class OpenLM_API_Service : ServiceBase
    {
        public OpenLM_API_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            GetSessionID(AdminAPIConnector.Instance);
        }

        protected override void OnStop()
        {
        }

        private string GetSessionID(AdminAPIConnector client)
        {
            var response = client.GetLoginFormSettings();
            if (response.UserAuthenticationRequired)
            {
                return AdminAPIAuthentication(client, response.ShowWinAuth);
            }
            else//No authentication required
            {
                WriteServiceResulToTextFile("No authentication required");
                return string.Empty;
            }
        }

        private static void WriteServiceResulToTextFile(string result)
        {
            File.WriteAllText(ConfigurationManager.AppSettings.Get("ResultFile"), $"{DateTime.Now.ToString()} {result}");
        }

        private static string AdminAPIAuthentication(AdminAPIConnector client, bool useWindowsAuthentication)
        {
            var userAuthenticationRequest = new APISamples.AdminAPI.UserAuthenticationRequest { TrustedAuthentication = useWindowsAuthentication };//Windows authentication 
            if (!useWindowsAuthentication)//OpenLM server authentication 
            {
                WriteServiceResulToTextFile("OpenLM server authentication required");
            }/* If OpenLM server authentication is required uncheck this code
            else
            {
                
                userAuthenticationRequest.UserName = UserName;
                userAuthenticationRequest.Password = Password;
                userAuthenticationRequest.TrustedAuthentication = false;
            }*/
            var userAuthenticationResponse = client.PerformUserAuthentication(userAuthenticationRequest);
            if (userAuthenticationResponse.Error != null)
            {
                WriteServiceResulToTextFile($"OpenLM server windows authentication error {userAuthenticationResponse.Error.Message}");
                throw new Exception(userAuthenticationResponse.Error.Message);
            }
            WriteServiceResulToTextFile($"OpenLM server windows authentication successful, created session id: {userAuthenticationResponse.SessionID}");
            return userAuthenticationResponse.SessionID;
        }
    }
}
