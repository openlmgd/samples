﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OpenlmExample
{
    public partial class UserDetailsForm : Form
    {
        public UserDetailsForm(UserDescription userDescription)
        {
            InitializeComponent();
            ClearAllData();
            SetData(userDescription);
        }

        private void ClearAllData()
        {
            labelTitle.Text = "";
            labelFirstName.Text = "";
            labelLastName.Text = "";
            labelDisplayName.Text = "";
            labelDepartment.Text = "";
            labelPhone.Text = "";
            labelDescription.Text = "";
            labelOffice.Text = "";
            labelEmail.Text = "";
            labelDefaultGroup.Text = "";
            labelDefaultProject.Text = "";
        }

        private void SetData(UserDescription userDescription)
        {
            if (userDescription.Details != null)
            {
                if (userDescription.Details.Title != null)
                {
                    labelTitle.Text = userDescription.Details.Title;
                }
                if (userDescription.Details.FirstName != null)
                {
                    labelFirstName.Text = userDescription.Details.FirstName;
                }
                if (userDescription.Details.LastName != null)
                {
                    labelLastName.Text = userDescription.Details.LastName;
                }
                if (userDescription.Details.DisplayName != null)
                {
                    labelDisplayName.Text = userDescription.Details.DisplayName;
                }
                if (userDescription.Details.Department != null)
                {
                    labelDepartment.Text = userDescription.Details.Department;
                }
                if (userDescription.Details.Phone != null)
                {
                    labelPhone.Text = userDescription.Details.Phone;
                }
                if (userDescription.Details.Description != null)
                {
                    labelDescription.Text = userDescription.Details.Description;
                }
                if (userDescription.Details.Office != null)
                {
                    labelOffice.Text = userDescription.Details.Office;
                }
                if (userDescription.Details.Email != null)
                {
                    labelEmail.Text = userDescription.Details.Email;
                }
                if (userDescription.Details.DefaultGroup != null)
                {
                    labelDefaultGroup.Text = userDescription.Details.DefaultGroup;
                }
                if (userDescription.Details.DefaultProject != null)
                {
                    labelDefaultProject.Text = userDescription.Details.DefaultProject;
                }
            }

            int i = 0;
            Color shaded = Color.FromArgb(240, 240, 240);
            if (userDescription.Projects != null)
            {
                foreach (UserProjects proj in userDescription.Projects)
                {
                    ListViewItem projectItem = new ListViewItem(proj.Project, 0);
                    if (i++ % 2 == 1)
                    {
                        projectItem.BackColor = shaded;
                        projectItem.UseItemStyleForSubItems = true;
                    }
                    
                    listViewProjects.Items.Add(projectItem);
                }
            }

            if (userDescription.Groups != null)
            {
                i = 0;
                foreach (UserGroups group in userDescription.Groups)
                {
                    ListViewItem groupItem = new ListViewItem(group.Group, 0);
                    if (i++ % 2 == 1)
                    {
                        groupItem.BackColor = shaded;
                        groupItem.UseItemStyleForSubItems = true;
                    }

                    listViewGroups.Items.Add(groupItem);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
