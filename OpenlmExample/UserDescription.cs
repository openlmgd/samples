﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace OpenlmExample
{
    [XmlRoot("ULM")]
    public class UserDescription
    {
        [System.Xml.Serialization.XmlElement("PARAMETERS", IsNullable = true)]
        public UserDetails Details { get; set; }

        [System.Xml.Serialization.XmlArray("GROUPS")]
        [System.Xml.Serialization.XmlArrayItem("GROUP")]
        public List<UserGroups> Groups {get;set;}

        [System.Xml.Serialization.XmlArray("PROJECTS")]
        [System.Xml.Serialization.XmlArrayItem("PROJECT")]
        public List<UserProjects> Projects { get; set; }

    }

    public class UserDetails
    {
        [System.Xml.Serialization.XmlElement("title", IsNullable = true)]
        public string Title { get; set; }

        [System.Xml.Serialization.XmlElement("first_name", IsNullable = true)]
        public string FirstName { get; set; }

        [System.Xml.Serialization.XmlElement("last_name", IsNullable = true)]
        public string LastName { get; set; }

        [System.Xml.Serialization.XmlElement("display_name", IsNullable = true)]
        public string DisplayName { get; set; }

        [System.Xml.Serialization.XmlElement("department", IsNullable = true)]
        public string Department { get; set; }

        [System.Xml.Serialization.XmlElement("phone_number", IsNullable = true)]
        public string Phone { get; set; }

        [System.Xml.Serialization.XmlElement("description", IsNullable = true)]
        public string Description { get; set; }

        [System.Xml.Serialization.XmlElement("office", IsNullable = true)]
        public string Office { get; set; }

        [System.Xml.Serialization.XmlElement("email", IsNullable = true)]
        public string Email { get; set; }

        [System.Xml.Serialization.XmlElement("default_group", IsNullable = true)]
        public string DefaultGroup { get; set; }

        [System.Xml.Serialization.XmlElement("default_project", IsNullable = true)]
        public string DefaultProject { get; set; }
    }

    public class UserProjects
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Project { get; set; }
    }

    public class UserGroups
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Group { get; set; }
    }
}
