﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.IO;
using OpenlmExample.SaasApi;

namespace OpenlmExample
{
    public partial class Openlm : Form
    {
        #region basic

        string _sessionID;
        private readonly Connector connector = ServerConnector.Instance;
        private Mode mode;

        public Openlm(Mode mode)
        {
            InitializeComponent();
            this.mode = mode;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (mode == Mode.Local)
            {
                _sessionID = "";

                AnalyzeMessage analyze = connector.GetLoginFormSettingsMessage() as AnalyzeMessage;
                if (analyze.MessageType == "ShowLoginForm")
                {
                    UserAuthentication form = new UserAuthentication(connector);
                    form.ShowDialog(this);
                }
                GetActiveProducts();
            }
            else
            {
                var saasClient = SaasConnector.Instance;
                UserAuthentication form = new UserAuthentication();
                form.ShowDialog(this);
                GetActiveProducts(saasClient);
            }
        }


        public void SetUserSessionID(string sessionID)
        {
            _sessionID = sessionID;
        }

        #endregion

        #region buttons click

        private void btnGetActiveProducts_Click(object sender, EventArgs e)
        {
            if (mode == Mode.Saas)
            {
                MessageBox.Show("Not implemented in Saas on demo.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    string chosen = listView1.SelectedItems[0].Text.Trim();
                    GetUserProducts(chosen);
                }
                else
                {
                    MessageBox.Show("You must choose user.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string chosen = listView1.SelectedItems[0].Text.Trim();
                GetUserDetails(chosen);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            GetActiveProducts();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                string chosen = listView1.SelectedItems[0].Text.Trim();
                SkypeMeassagesForm skypeForm = new SkypeMeassagesForm(chosen);
                skypeForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must choose user.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Active Products

        List<string> _users;

        ActiveProduct _activeProduct;

        //<ULM><MESSAGE type="GetActiveProducts" timeformat="dd/mm/yyyy hh:mm:ss" timezone="Israel Standard Time" /><SESSION sessionId="16122010095507384" /></ULM>
        public void GetActiveProducts()
        {
            AnalyzeMessage analyze = connector.GetActiveProductsMessage(_sessionID) as AnalyzeMessage;
            string messageType = analyze.MessageType;
            if (messageType == "ActiveProductsList")
            {
                _users = null;
                _activeProduct = null;
                _users = analyze.GetActiveProduct(out _activeProduct);
                listView1.Items.Clear();
                AddItems(_users);
            }
            else if (messageType == "Error")
            {
                string reason = analyze.AnalyzeErrorMessageReason().Trim();
                if (reason == "Session does not exist or expired. Please login")
                {
                    UserAuthentication form = new UserAuthentication(connector);
                    form.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show(reason, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void GetActiveProducts(SaasClient saasClient)
        {
            SaasConnector connector = new SaasConnector();
            var response =  connector.GetActiveProductsMessage(string.Empty) as List<string>;


            _users = null;
            _activeProduct = null;
            _users = response;

            listView1.Items.Clear();
            AddItems(_users);
        }

        private void AddItems(List<string> users)
        {
            int i = 0;
            Color shaded = Color.FromArgb(240, 240, 240);
            foreach (string user in users)
            {
                ListViewItem item = new ListViewItem(user, 0);
                if (i++ % 2 == 1)
                {
                    item.BackColor = shaded;
                    item.UseItemStyleForSubItems = true;
                }

                listView1.Items.Add(item);
            }
        }

        //<ULM><MESSAGE type="GetUserDescription" /><PARAMETERS><PARAM name="username">mushaphia</PARAM></PARAMETERS></ULM>
        private void GetUserDetails(string userName)
        {
            if (mode == Mode.Saas)
            {
                MessageBox.Show("Not implemented in Saas on demo.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {

                AnalyzeMessage analyze = connector.GetUserDescriptionMessage(userName) as AnalyzeMessage;
                string messageType = analyze.MessageType;
                if (messageType == "UserDescription")
                {
                    UserDescription userDescription = analyze.GetUserDescription();
                    if (userDescription != null)
                    {
                        //create form with user details
                        UserDetailsForm form = new UserDetailsForm(userDescription);
                        form.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("No details to show please contact system administrator", "Info!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (messageType == "Error")
                {
                    string reason = analyze.AnalyzeErrorMessageReason().Trim();
                    if (reason == "Session does not exist or expired. Please login")
                    {
                        UserAuthentication form = new UserAuthentication(connector);
                        form.ShowDialog(this);
                    }
                    else
                    {
                        MessageBox.Show(analyze.AnalyzeErrorMessageReason().Trim(), "Error!", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void GetUserProducts(string userName)
        {
            AnalyzeMessage analyze = new AnalyzeMessage();
            List<ActiveProductData> products = analyze.GetUserProducts(_activeProduct, userName);
            if (products.Count > 0)
            {
                //create form with user products
                UserActiveProducts form = new UserActiveProducts(userName, products);
                form.ShowDialog();
            }
            else
            {
                MessageBox.Show("User " + userName + " don't have products", "Info!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion
    }
}
