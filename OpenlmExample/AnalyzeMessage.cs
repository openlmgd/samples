﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;

namespace OpenlmExample
{
    public class AnalyzeMessage
    {
        #region basic analyze

        private XDocument _xmlDoc;
        private string _typeOfMessage;
        
        public AnalyzeMessage() { }
        
        public AnalyzeMessage(Stream response)
        {
            _typeOfMessage = "";
            try
            {
                _xmlDoc = XDocument.Load(XmlReader.Create(response));
                GetMessageType();
            }
            catch
            {
                _xmlDoc = null;
            }
        }

        private void GetMessageType()
        {
            if (_xmlDoc != null)
            {
                _typeOfMessage = _xmlDoc.Element("ULM").Element("MESSAGE").Attributes("type").First().Value;
            }
        }

        public string MessageType => _typeOfMessage;

        public string AnalyzeUserSessionIdAuthenticationProcess()
        {
            string sessionID = "";
            sessionID = _xmlDoc.Element("ULM").Element("SESSIONID").Value;
            return sessionID;
        }

        public string AnalyzeErrorMessageReason()
        {
            string reason = "";
            reason = _xmlDoc.Element("ULM").Element("MESSAGE").Value;
            return reason;
        }

        #endregion

        #region Active Products

        //XML response - <ULM><MESSAGE type="ActiveProductsList"><PRODUCTS><PRODUCT version="" username="aaron" vendor="ARCGIS" product="ARC/INFO" product_name="ArcInfo" start_time="1267638660" workstation="st999" duration="24690044.1297661" handle="4103" server_name="lakers" borrowed="False" linger_time="" linger_due="" user_idle_time="-" firstname="" lastname="" hasp_key="" /></PRODUCTS></MESSAGE></ULM> 
        public List<string> GetActiveProduct(out ActiveProduct activeProduct)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_xmlDoc.ToString());
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(typeof(ActiveProduct));
            object obj = ser.Deserialize(reader);

            // cast obj into UserDescription type
            activeProduct = (ActiveProduct)obj;
            
            List<string> retVal = new List<string>();

            var users = _xmlDoc.Element("ULM").Element("MESSAGE").Element("PRODUCTS").Elements("PRODUCT")
              .GroupBy(property => (string)property.Attribute("username"))
              .Select(g => g.First())
              .Select(f => new XElement("USER",
                      new XAttribute("name", f.Attribute("username").Value),
                      f.Attribute("value")));
            
            foreach (var name in users)
            {
                 retVal.Add(name.FirstAttribute.Value);
            }
            
            return retVal;
        }

        //XML response - <ULM><MESSAGE type="UserDescription" /><PARAMETERS><userid>2</userid><user_name>1dr</user_name><first_name>Dan</first_name><last_name>Gil</last_name><display_name>Dan</display_name><title>yyy</title><department>Devloper</department><phone_number>6378999</phone_number><description>Best Employer</description><office>office1</office><email>1dr@gmail.com</email><default_group>OpenLM Users</default_group><default_project>car project</default_project><enabled>True</enabled></PARAMETERS><PROJECTS><PROJECT id="2" name="phone project" /><PROJECT id="3" name="windows project" /></PROJECTS><GROUPS><GROUP id="2" name="GISapd" /><GROUP id="1" name="GISteam" /></GROUPS></ULM>
        public UserDescription GetUserDescription()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(_xmlDoc.ToString());
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
            XmlSerializer ser = new XmlSerializer(typeof(UserDescription));
            object obj = ser.Deserialize(reader);
            
            // cast obj into UserDescription type
            UserDescription userDescription = (UserDescription)obj;

            return userDescription;
        }

        public List<ActiveProductData> GetUserProducts(ActiveProduct products, string userName)
        {
            List<ActiveProductData> retVal = new List<ActiveProductData>();
            
            var userProducts =
              from p in products.Data.ActiveProductDataList
              where p.UserName == userName
              select p;

            foreach (ActiveProductData proData in userProducts)
            {
                retVal.Add(proData);
            }
            
            return retVal;
        }

        #endregion
        
    }
}
