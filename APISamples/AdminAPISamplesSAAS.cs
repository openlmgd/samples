using System;
using APISamples.SaasApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace APISamples
{
    public class AdminAPISamplesSAAS
    {
        private string Username = "Demo";
        private string Password = "DemoDemo321";
        [TestMethod]
        public void GetTop10DeniedFeaturesInLast30Days_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new DenialChartRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new[] { "value" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                },

                GetTrueDenialsOnly = true,
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                },
                GroupBy = "Feature"
            };

            var response = client.GetDenialsChart(request);

            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop5DeniedUsersInLast30Days_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new DenialChartRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 5,
                        Sort = new string[] { "value" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                },

                GetTrueDenialsOnly = true,
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                },
                GroupBy = "User"
            };

            var response = client.GetDenialsChart(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10MostUsedFeaturesInLast30Days_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });
            
            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new LicensesActivityRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new string[] { "usagetime" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                },

                GroupBy = "Feature",
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                }
            };

            var response = client.GetLicensesActivityByGroup(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10UsersOfMostUsedFeaturesInLast30Days_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new LicensesActivityRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new string[] { "usagetime" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                },

                GroupBy = "User",
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                }
            };

            var response = client.GetLicensesActivityByGroup(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10CurrentLongestIdleSessions_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var request = new CurrentlyConsumedLicensesRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new string[] { "idle_times" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                }
            };

            var response = client.GetCurrentlyConsumedLicenses(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10CurrentLongestUsageSessions_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var request = new CurrentlyConsumedLicensesRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new string[] { "duration" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                }
            };

            var response = client.GetCurrentlyConsumedLicenses(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetLicenseServersStatus_SAAS()
        {
            var client = new SaasApi.SaasClient();

            var loginResponse = client.SaasLogIn(new SaasLoginRequest()
            {
                Username = Username,
                Password = Password
            });

            var request = new GetLicenseServersRequest
            {
                SaasToken = loginResponse.SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                }
            };

            var response = client.GetLicenseServers(request);
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }
    }
}
