using APISamples.AdminAPI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace APISamples
{
    [TestClass]
    public class AdminAPISamples
    {
        private readonly AdminAPIConnector connector = AdminAPIConnector.Instance;

        [TestMethod]
        public void GetLicenses()
        {
            /* Old code for OpenLM server version 4.x or older for SOAP API call
            AdminAPIClient client = new AdminAPIClient();
            var request = new LicenseInfoRequest {
                BaseInfo = new RequestBaseInfo()
            };
            LicensesResponse response = client.GetLicenses(request);*/

            //Start code for converted API SOAP call for OpenLM server version 5.x
            var request = new LicenseInfoRequest
            {
                BaseInfo = connector.CreateBaseInfo()
            };
            LicensesResponse response = connector.Get<LicensesResponse, LicenseInfoRequest>(request, "GetLicenses");
            //End code for converted API SOAP call for OpenLM server version 5.x

            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetLicensesActivity()
        {
            /* Old code for OpenLM server version 4.x or older for SOAP API call
            AdminAPIClient client = new AdminAPIClient();
            var request = new LicensesActivityRequest {
                BaseInfo = new RequestBaseInfo()
            };
            LicensesActivityResponse response = client.GetLicensesActivity(request);*/

            //Start code for converted API SOAP call for OpenLM server version 5.x
            var request = new LicensesActivityRequest
            {
                BaseInfo = connector.CreateBaseInfo()
            };
            LicensesActivityResponse response = connector.Get<LicensesActivityResponse, LicensesActivityRequest>(request, "GetLicensesActivity");
            //End code for converted API SOAP call for OpenLM server version 5.x

            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10DeniedFeaturesInLast30Days()
        {
            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new DenialChartRequest
            {
                BaseInfo = connector.CreateBaseInfo(),

                GetTrueDenialsOnly = true,
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                },
                GroupBy = "Feature"
            };

            var response = connector.Get<DenialsChartResponse, DenialChartRequest>(request, "GetDenialsChart");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop5DeniedUsersInLast30Days()
        {
            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new DenialChartRequest
            {
                BaseInfo = connector.CreateBaseInfo(),

                GetTrueDenialsOnly = true,
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                },
                GroupBy = "User"
            };
            var response = connector.Get<DenialsChartResponse, DenialChartRequest>(request, "GetDenialsChart");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10MostUsedFeaturesInLast30Days()
        {
            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

                var request = new LicensesActivityRequest
            {
                BaseInfo = connector.CreateBaseInfo(),
                GroupBy = "Feature",
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day                    
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                }
            };

            var response = connector.Get<LicensesActivityByGroupResponse, LicensesActivityRequest>(request, "GetLicensesActivityByGroup");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10UsersOfMostUsedFeaturesInLast30Days()
        {
            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new LicensesActivityRequest
            {
                BaseInfo = connector.CreateBaseInfo(),
                GroupBy = "User",
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                }
            };

            var response = connector.Get<LicensesActivityByGroupResponse, LicensesActivityRequest>(request, "GetLicensesActivityByGroup");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10CurrentLongestIdleSessions()
        {
            var request = new CurrentlyConsumedLicensesRequest
            {
                BaseInfo = connector.CreateBaseInfo()
            };

            var response = connector.Get<CurrentlyConsumedLicensesResponse, CurrentlyConsumedLicensesRequest>(request, "GetCurrentlyConsumedLicenses");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetTop10CurrentLongestUsageSessions()
        {
            var request = new CurrentlyConsumedLicensesRequest
            {
                BaseInfo = connector.CreateBaseInfo()
            };

            var response = connector.Get<CurrentlyConsumedLicensesResponse, CurrentlyConsumedLicensesRequest>(request, "GetCurrentlyConsumedLicenses");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }

        [TestMethod]
        public void GetLicenseServersStatus()
        {
            var request = new GetLicenseServersRequest
            {
                BaseInfo = connector.CreateBaseInfo()
            };

            var response = connector.Get<LicenseServersResponse, GetLicenseServersRequest>(request, "GetLicenseServers");
            Assert.IsNotNull(response);
            Assert.IsNull(response.Error);
        }
    }
}